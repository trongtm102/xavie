(function($) {
	"use strict";
	$(document).ready(function() {

		/* [ Count Time to Date ]
		--------------------------------------*/
	  	// Time to
	  	var countDownDate = new Date("Dec 5, 2018 15:37:25").getTime();

	  	// Refrest Time after one second
	  	var x = setInterval(function() {

	    // Get Current Time
	    var now = new Date().getTime();

	    // Get Time Between Time To And Current Time
	    var distance = countDownDate - now;

	    // Count
	    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	    $( '.count-down-field.days' ).find( 'p' ).text( days );
	    $( '.count-down-field.hours' ).find( 'p' ).text( hours );
	    $( '.count-down-field.mins' ).find( 'p' ).text( minutes );
	    $( '.count-down-field.secs' ).find( 'p' ).text( seconds );
	    // Message When Time To = Current Time
	    if (distance < 0) {
	    	clearInterval( x );
	    	alert("Close");
	    } }, 1000);
	    
	  });
})(jQuery);