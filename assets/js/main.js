/*------------------------------------------------------------------
Project:    TMT
Version:    1.0
Last change:    26/11/2018
Assigned to:    
Primary use:    TMT 
-------------------------------------------------------------------*/


(function($) {
    "use strict";
    $(document).ready(function() {

        /*  [ Main Slider ]
        - - - - - - - - - - - - - - - - - - - - */
        $('.main-slider-area .owl-carousel').owlCarousel({
            loop:true,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            margin:0,
            dots: true,
            nav: false,
            items: 1,
        });

    });
})(jQuery);

//----- js menu
function changeClass(className, idName) {
    var element = document.getElementById(idName);
    const find = element.classList.contains(className);
    if(find)
        return element.classList.remove(className);
    return element.classList.add(className);
}
