document.addEventListener('DOMContentLoaded', function() {
  var mn = $('.header-main'),
  core = $('#main').eq(0),
  mns = 'header-fixed',
  bit, hdr;

  $(window).resize(function() {
    bit = mn.outerHeight();
    hdr = $('.header-top').outerHeight();
  })
  .resize().scroll(function() {
    if ($(this).scrollTop() > hdr) {
      mn.addClass(mns);
      core.css('margin-top', bit);
    } else {
      mn.removeClass(mns);
      core.attr('style', '');
    }
  })
  .on('load', function() {
    $(this).scroll();
  });
});